import 'dart:io';

import 'package:flutter/material.dart';
import 'package:firebase_ml_vision/firebase_ml_vision.dart';
import 'package:image_picker/image_picker.dart';

void main() {
  runApp(SampleApp());
}

class SampleApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Sample App',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: SampleAppPage(),
    );
  }
}

class SampleAppPage extends StatefulWidget {
  SampleAppPage({Key key}) : super(key: key);

  @override
  _SampleAppPageState createState() => _SampleAppPageState();
}

class _SampleAppPageState extends State<SampleAppPage> {

  String _text = 'not scanned yet';

  void _btnTap2() async{
    print("btn tap 2");
    _text = "test_text";
    setState(() {

    });
    File imageFile = await ImagePicker.pickImage(source: ImageSource.gallery);
    _text = imageFile.path;

    final FirebaseVisionImage visionImage = FirebaseVisionImage.fromFile(imageFile);
    final BarcodeDetector barcodeDetector = FirebaseVision.instance.barcodeDetector();
    List barcodeList = await barcodeDetector.detectInImage(visionImage);

    print("length");
    print(barcodeList.length);

    for (Barcode barcode in barcodeList) {
      print(barcode.displayValue);
      print(barcode.rawValue);
      _text = barcode.displayValue;
    }

    setState(() {

    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Sample App"),
      ),
      body: new Builder(
          builder: (BuildContext context) {
            return new Column(
              children: <Widget>[
                new RaisedButton(onPressed: _btnTap2, child: Text("open camera")),
                Text(_text)
              ],
            );
          }
      )
    );
  }
}
